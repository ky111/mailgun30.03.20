﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    public class factory
    {
        private static Type[] mytypes { get; set; }
        private static DALcontracts.ImplementAttribute myAttributes { get; set; }
        private static Dictionary<Type, Type> dic { get; set; }
        public static factory instance;
        private factory() { }

        public static void LoadFile()
        {
            dic = new Dictionary<Type, Type>();
            var execPath = AppDomain.CurrentDomain.BaseDirectory;
            var arrFiles = Directory.GetFiles(execPath+"/dlls");
            foreach (var assembly in arrFiles)
            {
                Assembly asm = Assembly.LoadFrom(assembly);
                mytypes = asm.GetTypes();
                foreach (var type in mytypes)
                {
                    if (type.IsClass)
                    {
                        myAttributes = type.GetCustomAttribute<DALcontracts.ImplementAttribute>();
                    }
                    if (myAttributes != null)
                    {           //שם ממשק
                        dic.Add(myAttributes.interfaceName, type);
                    }

                }
            }
        }


        public  T Resolve<T>() where T : class
        {
            var result = dic[typeof(T)];
            if (result != null)
            {
                return Activator.CreateInstance(dic[typeof(T)]) as T;
            }
            else
            {
                throw new Exception("No class was found to implement the requested interface");
            }

        }
       
        public static factory getInstance()
        {
            if (instance == null)
                instance = new factory();
            return instance;
        }

    }
}

