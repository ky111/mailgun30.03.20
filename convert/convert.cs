﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace convert
{
    public class convert
    {
        //public static List<object> convertDBsetToDTO<T>(DataSet dataset)
        //{
        //    var isObject = false;
        //    List<object> list = new List<object>();
        //    foreach (var a in System.Attribute.GetCustomAttributes(typeof(T)))
        //    {
        //        if (a.GetType() == typeof(DTO.DTOAttribute))//it means that it is an Object
        //        {
        //            isObject = true;
        //        }
        //    }
        //    if (isObject == true)
        //    {
        //        foreach (DataRow row in dataset.Tables[0].Rows)
        //        {
        //            T Instance = Activator.CreateInstance<T>();
        //            var properties = Instance.GetType().GetProperties();
        //            //for (var i = 0; i < properties.Length; i++)
        //            foreach (PropertyInfo item in properties)
        //            {
        //                if(item.GetCustomAttribute<notInSqlAttribute>() == null)
        //                {
        //                    var firstName = dataset.Tables[0].AsEnumerable()
        //                        .Where(f => f.Field<string>("ElementName") == item.Name).FirstOrDefault();
        //                    if (firstName != null)
        //                    {
        //                        item.SetValue(Instance, row[item.Name]);
        //                    }
        //                    else   
        //                    {
        //                        item.SetValue(Instance,null);
        //                    }
        //                }
        //            }
        //            list.Add(Instance);
        //        }
        //    }
        //    else
        //    {
        //        foreach (DataTable table in dataset.Tables)
        //        {
        //            foreach (DataRow row in table.Rows)
        //            {
        //                foreach (object item in row.ItemArray)
        //                {
        //                    list.Add(item);
        //                }
        //            }
        //        }

        //    }
        //    return list;
        //}

        public static object defaultFunc(string t)
        {
            if (t == "int" || t=="float")
            {
                return 0;
            }
            else if (t == "bool")
            {
                return false;
            }
            else
            {
                return null;
            }
        }

        public static List<object> convertDBsetToDTO<T>(DataSet dataset)
        {
            var isObject = false;
            List<object> list = new List<object>();
            foreach (var a in System.Attribute.GetCustomAttributes(typeof(T)))
            {
                if (a.GetType() == typeof(DTO.DTOAttribute))//it means that it is an Object
                {
                    isObject = true;
                }
            }
            if (isObject == true)
            {
                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    T Instance = Activator.CreateInstance<T>();
                    var properties = Instance.GetType().GetProperties();
                    foreach (var item in properties)
                    {
                        if (item.GetCustomAttribute<NotInSqlAttribute>() == null
                            //|| item.GetCustomAttribute<IdentityAttribute>() == null
                            )
                        {
                            try
                            {
                                if (row[item.Name] != null)
                                {
                                    item.SetValue(Instance, row[item.Name]);
                                }
                                else
                                {
                                    ///if the ds is not all initilize.
                                    string t1 = row[item.Name].GetType().Name;
                                    var def = defaultFunc(t1);
                                    item.SetValue(Instance, def);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("error convert" + e.Message);
                            }
                        }
                    }
                    list.Add(Instance);
                }
            }
            else
            {
                foreach (DataTable table in dataset.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        foreach (object item in row.ItemArray)
                        {
                            list.Add(item);
                        }
                    }
                }

            }
            return list;
        }



        public static List<Object> convertDBsetToDTO(DataSet dataset, Type t)
        {
            var isObject = false;
            List<Object> list = new List<Object>();
            foreach (var a in System.Attribute.GetCustomAttributes(t))
            {
                if (a.GetType() == typeof(DTO.DTOAttribute))//it means that it is an Object
                {
                    isObject = true;
                }
            }
            if (isObject == true)
            {
                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    var Instance = Activator.CreateInstance(t);
                    var properties = Instance.GetType().GetProperties();
                    //for (var i = 0; i < properties.Length; i++)
                    foreach (var item in properties)
                    {
                        item.SetValue(Instance, row[item.Name]);
                    }
                    list.Add(Instance);
                }
            }
            else
            {
                foreach (DataTable table in dataset.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        foreach (var item in row.ItemArray)//it was T but he didnt accept to get t
                        {
                            list.Add(item);
                        }
                    }
                }
            }
            return list;
        }
        public static List<T> convertDBsetToDTO<T>(List<DataSet> dataset)
        {
            var isObject = false;
            List<T> list = new List<T>();
            foreach (var a in Attribute.GetCustomAttributes(typeof(T)))
            {
                //bool hasDtoAttribute = typeof(T).GetCustomAttributes(typeof(DTO.DTOAttribute), true).Any();
                if (a.GetType() == typeof(DTO.DTOAttribute))
                {
                    isObject = true;
                }
            }
            if (isObject == true)
            {
                foreach (var iDS in dataset)
                {
                    foreach (DataRow row in iDS.Tables[0].Rows)
                    {

                        var Instance = Activator.CreateInstance<T>();
                        var properties = Instance.GetType().GetProperties();
                        //for (var i = 0; i < properties.Length; i++)
                        foreach (var item in properties)
                        {
                            if(item.GetCustomAttribute<NotInSqlAttribute>() == null)
                            {
                                item.SetValue(Instance, row[item.Name]);
                            }
                        }
                        list.Add(Instance);
                    }
                }
            }
            else
            {
                foreach (var a in dataset)
                {
                    foreach (DataTable table in a.Tables)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            foreach (T item in row.ItemArray)
                            {
                                list.Add(item);
                            }
                        }
                    }
                }
            }
            return list;
        }


        public static List<SqlParameter> convertDTOToSqlParameters<T>(T ds, string name = null)
        {
            var isObject = false;
            List<SqlParameter> l = new List<SqlParameter>();
            if (name == null)
            {
                IPAddresscs w = new IPAddresscs();
                if (typeof(T) == typeof(IPAddresscs))
                {
                    w = ds as IPAddresscs;
                }
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    if ((property.GetCustomAttribute<IdentityAttribute>() == null && property.GetCustomAttribute<NotInSqlAttribute>() == null)|| (property.GetCustomAttribute<IdentityAttribute>() != null && w.id>0))
                    {
                        l.Add(new SqlParameter("@" + property.Name, property.GetValue(ds)));
                    }
                }
            }
            else
            {
                l.Add(new SqlParameter("@" + name, ds));
            }
            return l;
        }
    }
}

