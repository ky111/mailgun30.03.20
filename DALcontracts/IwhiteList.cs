﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DALcontracts
{
    public interface IWhiteList
    {
        public DataSet addIpToDB(List<SqlParameter> listParameters, string con);
        public void deleteIP(List<SqlParameter> listParameters, string con);
        public void updateIP(List<SqlParameter> listParameters,string con );
        public DataSet getWhiteList(string con);
    }
}
