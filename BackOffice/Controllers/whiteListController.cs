﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DTO;
using DAL;
using DALcontracts;

namespace BackOffice.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class whiteListController : ControllerBase
    {
        string con;
        public whiteListController(string _con) { con = _con; }
        [HttpPost]
        [Route("{action}")]
        public List<int> addIP([FromBody] IPAddresscs wlDTO)
        {
            var IpParams = convert.convert.convertDTOToSqlParameters(wlDTO);
            var i = Factory.factory.getInstance().Resolve<IWhiteList>() as IWhiteList;
            var p = i.addIpToDB(IpParams, con);
            var c = convert.convert.convertDBsetToDTO<int>(p);
            List<int> c1 = new List<int>();
            foreach (var item in c)
            {
                c1.Add(Convert.ToInt32(item));
            }
            return c1;
        }


        [HttpGet]
        [Route("{action}")]
        public void deleteIP(int id)
        {
            var IpParams = convert.convert.convertDTOToSqlParameters(id,"Id");
            var i = Factory.factory.getInstance().Resolve<IWhiteList>() as IWhiteList;
            i.deleteIP(IpParams, con);
        }
        [HttpPost]
        [Route("{action}")]
        public void updateIP(IPAddresscs wlDTO)
        {
            var IpParams = convert.convert.convertDTOToSqlParameters(wlDTO);
            var i = Factory.factory.getInstance().Resolve<IWhiteList>() as IWhiteList;
            i.updateIP(IpParams, con);
        }
        [HttpGet]
        [Route("{action}")]
        public List<IPAddresscs> getWhiteList()
        {
            try
            {
                var i = Factory.factory.getInstance().Resolve<IWhiteList>() as IWhiteList;
                var list = i.getWhiteList(con);
                var c = convert.convert.convertDBsetToDTO<IPAddresscs>(list);
                List<IPAddresscs> c2 = new List<IPAddresscs>();
                foreach (var item in c)
                {
                    c2.Add(item as IPAddresscs);
                }
                return c2;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                
           
            }
            return null;
        }
        [HttpGet]
        [Route("{action}")]
        public ActionResult<int> GetAll()
        {
            return Ok(14);          
        }
    }
}
