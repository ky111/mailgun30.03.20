﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    [DTO]
    public class IPAddresscs
    {
        [Identity]
        public int id { get; set; }
        public string IPAddress { get; set; }
        public string DescriptionAbout { get; set; }
        [NotInSql]
        public Boolean isEdit { get; set; }
    }
    
   
}
