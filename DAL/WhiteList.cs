﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DALcontracts;

namespace DAL
{
    [ImplementAttribute(typeof(IWhiteList))]
    class WhiteList : IWhiteList
    {
        public DataSet addIpToDB(List<SqlParameter> listParameters, string sqlconnection)
        {
            using (SqlConnection con = new SqlConnection(sqlconnection))
            {
                SqlCommand cmd = new SqlCommand("addIp", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(listParameters.ToArray());
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }
        public void deleteIP(List<SqlParameter> listParameters, string sqlconnection)
        {
            using (SqlConnection con = new SqlConnection(sqlconnection))
            {
                SqlCommand cmd = new SqlCommand("deleteIp", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(listParameters.ToArray());
                try
                {
                    con.Open();
                    var r=cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }
        public void updateIP(List<SqlParameter> listParameters, string sqlconnection)
        {
            using (SqlConnection con = new SqlConnection(sqlconnection))
            {
                SqlCommand cmd = new SqlCommand("updateIp", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(listParameters.ToArray());
                try
                {
                    con.Open();
                    var r=cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }
        public DataSet getWhiteList(string sqlconnection)
        {
            using (SqlConnection con = new SqlConnection(sqlconnection))
            {
                SqlCommand cmd = new SqlCommand("getWhiteList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }
    }
}

