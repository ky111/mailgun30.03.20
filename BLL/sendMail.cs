﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using DTO;

namespace BLL
{
    public class sendMail
    {
        public static void sendEmail(SendRequest sendRequest)
        {
            MailMessage mail = new MailMessage();
            if (sendRequest.listTo != null)
            {
                foreach (var item in sendRequest.listTo)
                {
                    mail.To.Add(item);
                }
            }
            mail.From = new MailAddress(sendRequest.from);
            mail.Subject = sendRequest.subject;
            mail.Body = sendRequest.body;
            if (sendRequest.listAttchment != null)
            {
                foreach (var item in sendRequest.listAttchment)
                {
                    mail.Attachments.Add(new Attachment(item));
                }
            }
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
            SmtpServer.EnableSsl = true;
            SmtpServer.Credentials = new System.Net.NetworkCredential(mail.From.ToString(), sendRequest.password);
            try
            {
                //need to change to unsecure email address sender
                SmtpServer.Send(mail);
               
            }
            catch (Exception e)
            {
                throw;
            }
        }

       
    }
}
