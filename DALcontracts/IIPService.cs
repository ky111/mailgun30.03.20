﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DALcontracts
{
    public interface IIPService
    {
        public DataSet checkIp(List<SqlParameter> listParameters,string sqlConnection);
    }
}
