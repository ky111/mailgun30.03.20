﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace DTO
{
    public class SendRequest
    {
        public SendRequest() { }
        public string from { get; set; }
        public string password { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public List<string> listTo { get; set; }
        public List<string> listAttchment { get; set; }
    }
}
