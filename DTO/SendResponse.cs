﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class SendResponse
    {
        public SendResponse() { }
        public bool IsSent { get; set; }
    }
}
