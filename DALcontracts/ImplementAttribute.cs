﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DALcontracts
{
    public class ImplementAttribute:Attribute
    {
        public Type interfaceName { get; set; }
        public ImplementAttribute(Type _name)
        {
            interfaceName = _name;
        }
    }
}
