﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTO;
using BLL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using DALcontracts;
using System.Net;

namespace mailgunServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendEmailController : ControllerBase
    {
        string con;
        public SendEmailController(string _con) {
            con = _con;
        }
        // GET: api/SendEmail
        [HttpGet]
        public int Get()
        {       
            return 15;
        }

        [HttpPost]
        [Route("{action}")]
        public SendResponse sendMail(SendRequest sendRequest)
        {
            var ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList.GetValue(1).ToString();
            var s = convert.convert.convertDTOToSqlParameters(ip, "IpAddress");
            var f = Factory.factory.getInstance().Resolve<IIPService>() as IIPService;
            var ds = f.checkIp(s, con);
            var ipRes = convert.convert.convertDBsetToDTO<int>(ds);
            SendResponse sendResponse = new SendResponse();
            if(ipRes.Count>0)
            {
                try
                {
                    BLL.sendMail.sendEmail(sendRequest);
                    sendResponse.IsSent = true;
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine("email not sent" + e.Message);
                }
            }
            else
            {
                sendResponse.IsSent = false;
            }


            return sendResponse;
        }
    }

   
}
