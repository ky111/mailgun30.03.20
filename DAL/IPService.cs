﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DALcontracts;

namespace DAL
{
    [ImplementAttribute(typeof(IIPService))]
    public class IPService:IIPService
    {
        public DataSet checkIp(List<SqlParameter> listParameters,string sqlConnection)
        {
            using (SqlConnection con = new SqlConnection(sqlConnection))
            {
                SqlCommand cmd = new SqlCommand("checkIpAddress", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(listParameters.ToArray());
                DataSet ds = new DataSet();
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = cmd;
                    ad.Fill(ds);
                }
                catch (Exception e)
                {
                    Console.WriteLine("cant open connection" + e.Message);
                }
                finally
                {
                    con.Close();
                }
                return ds;
            }
        }
    }
}
